<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Puppybook</title>

    <link rel="stylesheet" href="css/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bower_components/angular-bootstrap/ui-bootstrap-csp.css">
    <link rel="stylesheet" href="bower_components/ngToast/dist/ngToast.min.css">
    <link rel="stylesheet" href="partials/home/home.css">
    <link rel="stylesheet" href="partials/lost-dogs/lost-dogs.css">
    <link rel="stylesheet" href="partials/login/login.css">
    <link rel="stylesheet" href="partials/register/register.css">
    <link rel="stylesheet" href="partials/profile/view-profile/profile.css">
    <link rel="stylesheet" href="partials/profile/my-puppies/my-puppies.css">
    <link rel="stylesheet" href="partials/profile/my-lost-friends/my-lost-friends.css">
    <link rel="stylesheet" href="partials/profile/modals/add-post-modal/add-post-modal.css">
    <link rel="stylesheet" href="partials/profile/modals/edit-post-modal/edit-post-modal.css">
    <link rel="stylesheet" href="partials/profile/modals/add-lost-friends-modal/add-lost-friends-modal.css">
    <link rel="stylesheet" href="partials/profile/modals/edit-lost-friends-modal/edit-lost-friends-modal.css">
    <link rel="stylesheet" href="partials/nav-menu/nav-menu.css">
</head>

<body>
    <toast></toast>
    <ui-view></ui-view>
</body>

    <script src="bower_components/angular/angular.min.js"></script>
    <script src="bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="bower_components/angular-animate/angular-animate.min.js"></script>
    <script src="bower_components/angular-sanitize/angular-sanitize.min.js"></script>
    <script src="bower_components/angular-css/angular-css.min.js"></script>
    <script src="bower_components/ngstorage/ngStorage.min.js"></script>
    <script src="bower_components/ngToast/dist/ngToast.min.js"></script>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
    <script src="bower_components/angular-spinner/dist/angular-spinner.min.js"></script>

    <!-- Main js -->
    <script src="js/app.js"></script>
    <script src="js/global.controller.js"></script>
    <script src="js/constants.js"></script>
    <script src="js/http.interceptor.js"></script>

    <!-- Controllers -->
    <script src="partials/home/home.controller.js"></script>
    <script src="partials/lost-dogs/lost-dogs.controller.js"></script>
    <script src="partials/login/login.controller.js"></script>
    <script src="partials/register/register.controller.js"></script>
    <script src="partials/profile/view-profile/profile.controller.js"></script>
    <script src="partials/profile/my-puppies/my-puppies.controller.js"></script>
    <script src="partials/profile/my-lost-friends/my-lost-friends.controller.js"></script>
    <script src="partials/profile/modals/add-post-modal/add-post-modal.controller.js"></script>
    <script src="partials/profile/modals/add-lost-friends-modal/add-lost-friends-modal.controller.js"></script>
    <script src="partials/profile/modals/edit-post-modal/edit-post-modal.controller.js"></script>
    <script src="partials/profile/modals/edit-lost-friends-modal/edit-lost-friends-modal.controller.js"></script>

    <!-- Services -->
    <script src="js/global-services/auth.service.js"></script>
    <script src="partials/home/home.service.js"></script>
    <script src="partials/lost-dogs/lost-dogs.service.js"></script>
    <script src="partials/register/register.service.js"></script>
    <script src="partials/profile/view-profile/profile.service.js"></script>
    <script src="partials/profile/my-puppies/my-puppies.service.js"></script>
    <script src="partials/profile/my-lost-friends/my-lost-friends.service.js"></script>
    <script src="partials/single-post/single-post.service.js"></script>
    <script src="partials/single-lost-dog/single-lost-dog.service.js"></script>
    <script src="partials/profile/modals/add-post-modal/add-post-modal.service.js"></script>
    <script src="partials/profile/modals/add-lost-friends-modal/add-lost-friends-modal.service.js"></script>
    <script src="partials/profile/modals/edit-post-modal/edit-post-modal.service.js"></script>
    <script src="partials/profile/modals/edit-lost-friends-modal/edit-lost-friends-modal.service.js"></script>
</html>
