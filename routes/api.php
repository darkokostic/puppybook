<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Authentication
Route::post('/login', 'AuthenticationController@login');
Route::post('/logout', 'AuthenticationController@logout');
Route::post('/register', 'UserController@store');

// Users
Route::resource('/users', 'UserController',
    ['only' => ['index', 'update', 'show']]);
Route::post('/users', 'UserController@createUser');
Route::get('/users/{id}/posts', 'UserController@getPosts');
Route::get('/users/{id}/lost-dogs', 'UserController@getLostDogs');
Route::post('/users/delete/{id}', 'UserController@destroy');
Route::get('/search/users', 'UserController@search');

// Posts
Route::resource('/posts', 'PostController',
    ['only' => ['index', 'store', 'update', 'show']]);
Route::post('/posts/delete/{id}', 'PostController@destroy');
Route::get('/search/posts', 'PostController@search');

// Lost Dogs
Route::resource('/lost-dogs', 'LostDogController',
    ['only' => ['index', 'store', 'update', 'show']]);
Route::post('/lost-dogs/delete/{id}', 'LostDogController@destroy');
Route::get('/search/lost-dogs', 'LostDogController@search');
