angular
    .module('app')
    .controller('HomeCtrl', HomeCtrl);

function HomeCtrl($scope, HomeService, Constants) {
    $scope.isLoading = true;
    $scope.getData = function(page) {
        HomeService.getData(page)
            .then(function (response) {
                $scope.isLoading = false;
                if(response.entity) {
                    $scope.posts = response.entity.data;
                    $scope.totalItems = response.entity.total;
                    $scope.currentPage = response.entity.current_page;
                    $scope.perPage = response.entity.per_page;
                } else {
                    $scope.posts = response.entity;
                }
            })
            .catch(function () {
                $scope.isLoading = false;
            });
    };

    $scope.getData(1);
    $scope.hostUrl = Constants.HOST_URL;

    $scope.paginate = function(currentPage) {
        $scope.getData(currentPage);
    }
}