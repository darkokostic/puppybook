angular
    .module('app')
    .service('HomeService', HomeService);

function HomeService($q, $http, Constants) {
    function getData(page) {
        var deffered = $q.defer();
        $http.get(Constants.ENDPOINT_URL + Constants.GET_POSTS_API + "?page=" + page)
            .then(function (response) {
                deffered.resolve(response.data);
            })
            .catch(function (error) {
                deffered.reject(error.data);
            });

        return deffered.promise;
    }

    return {
        getData: getData
    }
}