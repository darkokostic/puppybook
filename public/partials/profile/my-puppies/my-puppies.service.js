angular
    .module('app')
    .service('MyPuppiesService', MyPuppiesService);

function MyPuppiesService($q, $http, Constants, $localStorage) {
    function getData(page) {
        var deffered = $q.defer();
        $http.get(Constants.ENDPOINT_URL + Constants.GET_USERS_API + "/" + $localStorage.user.id + "/" + Constants.GET_POSTS_API + "?page=" + page)
            .then(function (response) {
                deffered.resolve(response.data);
            })
            .catch(function (error) {
                deffered.reject(error.data);
            });

        return deffered.promise;
    }

    function deletePost(id) {
        var deffered = $q.defer();
        $http.post(Constants.ENDPOINT_URL + Constants.GET_POSTS_API + "/" + Constants.DELETE_API + "/" + id)
            .then(function (response) {
                deffered.resolve(response.data);
            })
            .catch(function (error) {
                deffered.reject(error.data);
            });

        return deffered.promise;
    }

    return {
        getData: getData,
        deletePost: deletePost
    }
}