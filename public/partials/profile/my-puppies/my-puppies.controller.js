angular
    .module('app')
    .controller('MyPuppiesCtrl', MyPuppiesCtrl);

function MyPuppiesCtrl($scope, MyPuppiesService, Constants, ngToast, $uibModal) {
    $scope.isLoading = true;
    $scope.page = 1;
    $scope.getData = function(page) {
        $scope.page = page;
        MyPuppiesService.getData(page)
            .then(function (response) {
                $scope.isLoading = false;
                if(response.entity) {
                    $scope.posts = response.entity.data;
                    $scope.totalItems = response.entity.total;
                    $scope.currentPage = response.entity.current_page;
                    $scope.perPage = response.entity.per_page;
                } else {
                    $scope.posts = response.entity;
                }
            })
            .catch(function () {
                $scope.isLoading = false;
            });
    };

    $scope.getData($scope.page);
    $scope.hostUrl = Constants.HOST_URL;

    $scope.paginate = function(currentPage) {
        $scope.getData(currentPage);
    };

    $scope.delete = function(id) {
        MyPuppiesService.deletePost(id)
            .then(function (response) {
                ngToast.success({
                    content: response.message
                });
                if($scope.posts.length > 1) {
                    $scope.getData($scope.page);
                } else {
                    if($scope.page != 1) {
                        $scope.page--;
                    }
                    $scope.getData($scope.page);
                }
            })
    };

    $scope.openAddModal = function() {
        var addModal = $uibModal.open({
            animation: true,
            templateUrl: 'partials/profile/modals/add-post-modal/add-post-modal.view.html',
            controller: 'AddNewPostCtrl'
        });
        addModal.result
            .then(function () {
                $scope.getData($scope.page);
            })
    };

    $scope.openEditModal = function(post) {
        var editModal = $uibModal.open({
            animation: true,
            templateUrl: 'partials/profile/modals/edit-post-modal/edit-post-modal.view.html',
            controller: 'EditPostCtrl',
            resolve: {
                post: function () {
                    return post;
                }
            }
        });
        editModal.result
            .then(function () {
                $scope.getData($scope.page);
            })
    };
}