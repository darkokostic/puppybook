angular
    .module('app')
    .service('EditLostFriendsService', EditLostFriendsService);

function EditLostFriendsService($q, $http, Constants) {
    function editLostDog(post) {
        var deffered = $q.defer();
        $http.put(Constants.ENDPOINT_URL + Constants.GET_LOST_DOGS_API + "/" + post.id, post)
            .then(function (response) {
                deffered.resolve(response.data);
            })
            .catch(function (error) {
                deffered.reject(error.data);
            });

        return deffered.promise;
    }

    return {
        editLostDog: editLostDog
    }
}