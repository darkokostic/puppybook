angular
    .module('app')
    .controller('EditLostFriendsCtrl', EditLostFriendsCtrl);

function EditLostFriendsCtrl($scope, ngToast, EditLostFriendsService, SingleLostDogService, $uibModalInstance, post) {
    SingleLostDogService.getData(post)
        .then(function (response) {
            $scope.post = response.entity;
        });
    $scope.edit = function(post) {
        $scope.isLoading = true;
        EditLostFriendsService.editLostDog(post)
            .then(function (response) {
                $scope.isLoading = false;
                ngToast.success({
                    content: response.message
                });
                $uibModalInstance.close();
            })
            .catch(function () {
                $scope.isLoading = false;
                $uibModalInstance.dismiss();
            });
    };
}