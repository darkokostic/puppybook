angular
    .module('app')
    .controller('AddLostFriendsCtrl', AddLostFriendsCtrl);

function AddLostFriendsCtrl($scope, ngToast, AddLostFriendsService, $uibModalInstance) {
    $scope.add = function(post) {
        $scope.isLoading = true;
        AddLostFriendsService.addNewPost(post)
            .then(function (response) {
                $scope.isLoading = false;
                ngToast.success({
                    content: response.message
                });
                $uibModalInstance.close();
            })
            .catch(function () {
                $scope.isLoading = false;
                $uibModalInstance.dismiss();
            });
    };
}