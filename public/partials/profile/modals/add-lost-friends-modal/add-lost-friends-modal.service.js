angular
    .module('app')
    .service('AddLostFriendsService', AddLostFriendsService);

function AddLostFriendsService($q, $http, Constants) {
    function addNewPost(post) {
        var deffered = $q.defer();
        $http.post(Constants.ENDPOINT_URL + Constants.GET_LOST_DOGS_API, post)
            .then(function (response) {
                deffered.resolve(response.data);
            })
            .catch(function (error) {
                deffered.reject(error.data);
            });

        return deffered.promise;
    }

    return {
        addNewPost: addNewPost
    }
}