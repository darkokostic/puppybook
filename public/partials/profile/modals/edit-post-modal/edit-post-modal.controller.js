angular
    .module('app')
    .controller('EditPostCtrl', EditPostCtrl);

function EditPostCtrl($scope, ngToast, EditPostService, SinglePostService, $uibModalInstance, post) {
    SinglePostService.getData(post)
        .then(function (response) {
            $scope.post = response.entity;
        });
    $scope.edit = function(post) {
        $scope.isLoading = true;
        EditPostService.editPuppy(post)
            .then(function (response) {
                $scope.isLoading = false;
                ngToast.success({
                    content: response.message
                });
                $uibModalInstance.close();
            })
            .catch(function () {
                $scope.isLoading = false;
                $uibModalInstance.dismiss();
            });
    };
}