angular
    .module('app')
    .service('EditPostService', EditPostService);

function EditPostService($q, $http, Constants) {
    function editPuppy(post) {
        var deffered = $q.defer();
        $http.put(Constants.ENDPOINT_URL + Constants.GET_POSTS_API + "/" + post.id, post)
            .then(function (response) {
                deffered.resolve(response.data);
            })
            .catch(function (error) {
                deffered.reject(error.data);
            });

        return deffered.promise;
    }

    return {
        editPuppy: editPuppy
    }
}