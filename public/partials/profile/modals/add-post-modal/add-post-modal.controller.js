angular
    .module('app')
    .controller('AddNewPostCtrl', AddNewPostCtrl);

function AddNewPostCtrl($scope, ngToast, AddNewPostService, $uibModalInstance) {
    $scope.add = function(post) {
        $scope.isLoading = true;
        AddNewPostService.addNewPuppy(post)
            .then(function (response) {
                $scope.isLoading = false;
                ngToast.success({
                    content: response.message
                });
                $uibModalInstance.close();
            })
            .catch(function () {
                $scope.isLoading = false;
                $uibModalInstance.dismiss();
            });
    };
}