angular
    .module('app')
    .service('AddNewPostService', AddNewPostService);

function AddNewPostService($q, $http, Constants) {
    function addNewPuppy(post) {
        var deffered = $q.defer();
        $http.post(Constants.ENDPOINT_URL + Constants.GET_POSTS_API, post)
            .then(function (response) {
                deffered.resolve(response.data);
            })
            .catch(function (error) {
                deffered.reject(error.data);
            });

        return deffered.promise;
    }

    return {
        addNewPuppy: addNewPuppy
    }
}