angular
    .module('app')
    .controller('MyLostFriendsCtrl', MyLostFriendsCtrl);

function MyLostFriendsCtrl($scope, MyLostFriendsService, Constants, ngToast, $uibModal) {
    $scope.isLoading = true;
    $scope.page = 1;
    $scope.getData = function(page) {
        $scope.page = page;
        MyLostFriendsService.getData(page)
            .then(function (response) {
                $scope.isLoading = false;
                if(response.entity) {
                    $scope.posts = response.entity.data;
                    $scope.totalItems = response.entity.total;
                    $scope.currentPage = response.entity.current_page;
                    $scope.perPage = response.entity.per_page;
                } else {
                    $scope.posts = response.entity;
                }
            })
            .catch(function () {
                $scope.isLoading = false;
            });
    };

    $scope.getData($scope.page);
    $scope.hostUrl = Constants.HOST_URL;

    $scope.paginate = function(currentPage) {
        $scope.getData(currentPage);
    };

    $scope.delete = function(id) {
        MyLostFriendsService.deletePost(id)
            .then(function (response) {
                ngToast.success({
                    content: response.message
                });
                if($scope.posts.length > 1) {
                    $scope.getData($scope.page);
                } else {
                    if($scope.page != 1) {
                        $scope.page--;
                    }
                    $scope.getData($scope.page);
                }
            })
    };

    $scope.openAddModal = function() {
        var addModal = $uibModal.open({
            animation: true,
            templateUrl: 'partials/profile/modals/add-lost-friends-modal/add-lost-friends-modal.view.html',
            controller: 'AddLostFriendsCtrl'
        });
        addModal.result
            .then(function () {
                $scope.getData($scope.page);
            })
    };

    $scope.openEditModal = function(post) {
        var editModal = $uibModal.open({
            animation: true,
            templateUrl: 'partials/profile/modals/edit-lost-friends-modal/edit-lost-friends-modal.view.html',
            controller: 'EditLostFriendsCtrl',
            resolve: {
                post: function () {
                    return post;
                }
            }
        });
        editModal.result
            .then(function () {
                $scope.getData($scope.page);
            })
    };
}