angular
    .module('app')
    .service('MyLostFriendsService', MyLostFriendsService);

function MyLostFriendsService($q, $http, Constants, $localStorage) {
    function getData(page) {
        var deffered = $q.defer();
        $http.get(Constants.ENDPOINT_URL + Constants.GET_USERS_API + "/" + $localStorage.user.id + "/" + Constants.GET_LOST_DOGS_API + "?page=" + page)
            .then(function (response) {
                deffered.resolve(response.data);
            })
            .catch(function (error) {
                deffered.reject(error.data);
            });

        return deffered.promise;
    }

    function deletePost(id) {
        var deffered = $q.defer();
        $http.post(Constants.ENDPOINT_URL + Constants.GET_LOST_DOGS_API + "/" + Constants.DELETE_API + "/" + id)
            .then(function (response) {
                deffered.resolve(response.data);
            })
            .catch(function (error) {
                deffered.reject(error.data);
            });

        return deffered.promise;
    }

    function addNewPost(post) {
        var deffered = $q.defer();
        $http.post(Constants.ENDPOINT_URL + Constants.GET_LOST_DOGS_API, post)
            .then(function (response) {
                deffered.resolve(response.data);
            })
            .catch(function (error) {
                deffered.reject(error.data);
            });

        return deffered.promise;
    }

    return {
        getData: getData,
        deletePost: deletePost,
        addNewPost: addNewPost
    }
}