angular
    .module('app')
    .service('SingleLostDogService', SingleLostDogService);

function SingleLostDogService($q, $http, Constants) {
    function getData(post) {
        var deffered = $q.defer();
        $http.get(Constants.ENDPOINT_URL + Constants.GET_LOST_DOGS_API + "/" + post.id)
            .then(function (response) {
                deffered.resolve(response.data);
            })
            .catch(function (error) {
                deffered.reject(error.data);
            });

        return deffered.promise;
    }

    return {
        getData: getData
    }
}