angular
    .module('app')
    .service('RegisterService', RegisterService);

function RegisterService($q, $http, Constants, $localStorage, $rootScope) {
    function register(credentials) {
        var deferred = $q.defer();
        $http.post(Constants.ENDPOINT_URL + Constants.REGISTER_API, credentials)
            .then(function (response) {
                $localStorage.token = response.data.entity.token;
                $localStorage.user = response.data.entity.user;
                $rootScope.isLoggedIn = true;
                $rootScope.user = $localStorage.user;
                deferred.resolve(response.data);
            })
            .catch(function (error) {
                deferred.reject(error.data);
            });

        return deferred.promise;
    }

    return {
        register: register
    }
}