angular
    .module('app')
    .controller('RegisterCtrl', RegisterCtrl);

function RegisterCtrl($scope, RegisterService, ngToast, $state) {
    console.log("Darko");
    $scope.isLoading = false;
    $scope.register = function(credentials) {
        $scope.isLoading = true;
        RegisterService.register(credentials)
            .then(function (response) {
                console.log(response);
                $scope.isLoading = false;
                ngToast.success({
                    content: response.message
                });
                $state.go('menu.home');
            })
            .catch(function (error) {
                $scope.isLoading = false;
            });
    }
}