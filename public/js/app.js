angular.module('app', [
    'ui.router',
    'angularCSS',
    'ngStorage',
    'ngToast',
    'ui.bootstrap',
    'angularSpinner'
])
    
.run(function ($rootScope, $localStorage, $transitions, $state, $q) {
    $rootScope.isLoggedIn = $localStorage.token != undefined;
    if($rootScope.isLoggedIn) {
        $rootScope.user = $localStorage.user;
    }
    $transitions.onBefore({to: 'menu.login'}, function() {
        var deferred = $q.defer();
        if($localStorage.token) {
            var params = { reload: true };
            deferred.resolve($state.target('menu.home', undefined, params));
        } else {
            deferred.resolve();
        }
        return deferred.promise;
    });

    $transitions.onBefore({to: 'menu.register'}, function() {
        var deferred = $q.defer();
        if($localStorage.token) {
            var params = { reload: true };
            deferred.resolve($state.target('menu.home', undefined, params));
        } else {
            deferred.resolve();
        }
        return deferred.promise;
    });

    $transitions.onBefore({to: 'menu.profile'}, function() {
        var deferred = $q.defer();
        if(!$localStorage.token) {
            var params = { reload: true };
            deferred.resolve($state.target('menu.login', undefined, params));
        } else {
            deferred.resolve();
        }
        return deferred.promise;
    });

    $transitions.onBefore({to: 'menu.my-lost-friends'}, function() {
        var deferred = $q.defer();
        if(!$localStorage.token) {
            var params = { reload: true };
            deferred.resolve($state.target('menu.login', undefined, params));
        } else {
            deferred.resolve();
        }
        return deferred.promise;
    });

    $transitions.onBefore({to: 'menu.my-puppies'}, function() {
        var deferred = $q.defer();
        if(!$localStorage.token) {
            var params = { reload: true };
            deferred.resolve($state.target('menu.login', undefined, params));
        } else {
            deferred.resolve();
        }
        return deferred.promise;
    });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider, ngToastProvider, usSpinnerConfigProvider) {
    $httpProvider.interceptors.push('httpInterceptor');

    ngToastProvider.configure({
        verticalPosition: 'bottom',
        horizontalPosition: 'right',
        animation: 'fade'
    });

    usSpinnerConfigProvider.setTheme('big', {color: '#555', radius: 20, length: 15});
    usSpinnerConfigProvider.setTheme('small', {color: '#555', radius: 8});

    $urlRouterProvider.otherwise('/');

    $stateProvider.
    state('menu', {
        abstract: true,
        templateUrl: 'partials/nav-menu/nav-menu.view.html',
        css: 'partials/nav-menu/nav-menu.css',
        controller: 'GlobalController'
    });

    $stateProvider.
    state('menu.home', {
        url: '/',
        views: {
            'menuContent': {
                templateUrl: 'partials/home/home.view.html',
                css: 'partials/home/home.css',
                controller: 'HomeCtrl'
            }
        }
    });

    $stateProvider.
    state('menu.lost-dogs', {
        url: '/lost-dogs',
        views: {
            'menuContent': {
                templateUrl: 'partials/lost-dogs/lost-dogs.view.html',
                css: 'partials/lost-dogs/lost-dogs.css',
                controller: 'LostDogsCtrl'
            }
        }
    });

    $stateProvider.
    state('menu.login', {
        url: '/login',
        views: {
            'menuContent': {
                templateUrl: 'partials/login/login.view.html',
                css: 'partials/login/login.css',
                controller: 'LoginCtrl'
            }
        }
    });

    $stateProvider.
    state('menu.register', {
        url: '/register',
        views: {
            'menuContent': {
                templateUrl: 'partials/register/register.view.html',
                css: 'partials/register/register.css',
                controller: 'RegisterCtrl'
            }
        }
    });

    $stateProvider.
    state('menu.profile', {
        url: '/profile',
        views: {
            'menuContent': {
                templateUrl: 'partials/profile/view-profile/profile.view.html',
                css: 'partials/profile/view-profile/profile.css',
                controller: 'ProfileCtrl'
            }
        }
    });

    $stateProvider.
    state('menu.my-puppies', {
        url: '/my-puppies',
        views: {
            'menuContent': {
                templateUrl: 'partials/profile/my-puppies/my-puppies.view.html',
                css: 'partials/profile/my-puppies/my-puppies.css',
                controller: 'MyPuppiesCtrl'
            }
        }
    });

    $stateProvider.
    state('menu.my-lost-friends', {
        url: '/my-lost-friends',
        views: {
            'menuContent': {
                templateUrl: 'partials/profile/my-lost-friends/my-lost-friends.view.html',
                css: 'partials/profile/my-lost-friends/my-lost-friends.css',
                controller: 'MyLostFriendsCtrl'
            }
        }
    });
});