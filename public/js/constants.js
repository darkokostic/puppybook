angular
    .module('app')
    .constant('Constants', {
        HOST_URL: "http://192.168.1.9:8000/",
        ENDPOINT_URL: "http://192.168.1.9:8000/api/",
        LOGIN_API: "login",
        LOGOUT_API: "logout",
        REGISTER_API: "register",
        GET_POSTS_API: "posts",
        GET_LOST_DOGS_API: "lost-dogs",
        GET_USERS_API: "users",
        DELETE_API: "delete"
    });