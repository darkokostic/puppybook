angular
    .module('app')
    .controller('GlobalController', GlobalController);

function GlobalController($scope, AuthService, $state, ngToast) {
    $scope.logout = function () {
        AuthService.logout()
            .then(function (response) {
                ngToast.success({
                    content: response.message
                });
                $state.go('menu.home');
            })
    }
}