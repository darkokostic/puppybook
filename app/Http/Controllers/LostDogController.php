<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\ImageService;
use App\Helpers\TokenHelper;
use App\Http\Requests\LostDog\CreateLostDogRequest;
use App\Http\Requests\LostDog\UpdateLostDogRequest;
use App\Image;
use App\LostDog;
use Illuminate\Http\Request;

class LostDogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lostDogs = LostDog::with('user', 'images')->latest()->paginate(12);
        if(count($lostDogs)) {
            return response()->custom(200, "Successfully get lost dogs", $lostDogs);
        }
        return response()->custom(200, "Successfully get lost dogs", null);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLostDogRequest $request)
    {
        if(TokenHelper::isValid()) {
            try {
                $lostDog = new LostDog();
                $lostDog->fill(request()->all());
                $lostDog->user_id = TokenHelper::getUserFromToken()->id;
                if($lostDog->save()) {
                    $image = new Image();
                    $image->imageable_id = $lostDog->id;
                    $image->imageable_type = Constant::LOST_DOG_IDENTIFIER;
                    if($request->hasFile('image')) {
                        $image->path = ImageService::upload($request->file('image'), Constant::LOST_DOGS_IMAGES_ROOT_PATH, $lostDog->id, $request->file('image')->getClientOriginalName());
                    } else {
                        $image->path = Constant::DEFAULT_POST_IMAGE_PATH;
                    }
                    $image->save();
                    return response()->custom(200, "Successfully created lost dog", LostDog::with('user', 'images')->find($lostDog->id));
                }
            } catch (\Exception $e) {
                return response()->custom(400, $e->getMessage(), null);
            }
        } else {
            return TokenHelper::invalidTokenResponse();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LostDog  $lostDog
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lostDog = LostDog::with('user', 'images')->find($id);
        if($lostDog) {
            return response()->custom(200, "Successfully get lost dog", $lostDog);
        }
        return response()->custom(404, "Lost dog not found", null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LostDog  $lostDog
     * @return \Illuminate\Http\Response
     */
    public function edit(LostDog $lostDog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LostDog  $lostDog
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLostDogRequest $request, $id)
    {
        if(TokenHelper::isValid()) {
            try {
                $lostDog = LostDog::find($id);
                if($lostDog) {
                    if(TokenHelper::getUserFromToken()->id == $lostDog->user_id) {
                        $lostDog->fill($request->all());
                        if($request->hasFile('image')) {
                            $lostDog_image = Image::where([['imageable_id', '=', $lostDog->id], ['imageable_type', '=', Constant::LOST_DOG_IDENTIFIER]])->first();
                            if($lostDog_image && $lostDog_image->path != Constant::DEFAULT_POST_IMAGE_PATH) {
                                ImageService::delete($lostDog_image->path, Constant::LOST_DOGS_IMAGES_ROOT_PATH, $lostDog->id);
                            }
                            $lostDog_image->path = ImageService::upload($request->file('image'), Constant::LOST_DOGS_IMAGES_ROOT_PATH, $lostDog->id, $request->file('image')->getClientOriginalName());
                            $lostDog_image->save();
                        }
                        $lostDog->save();
                        return response()->custom(200, 'Successfully updated lost dog', LostDog::with('user', 'images')->find($lostDog->id));
                    }
                    return response()->custom(403, 'You don\'t have permission to do this', null);
                }
                return response()->custom(404, "Lost dog not found", null);
            } catch (\Exception $e) {
                return response()->custom(400, $e->getMessage(), null);
            }
        } else {
            return TokenHelper::invalidTokenResponse();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LostDog  $lostDog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(TokenHelper::isValid()) {
            try {
                $lostDog = LostDog::find($id);
                if($lostDog) {
                    if(TokenHelper::getUserFromToken()->id == $lostDog->user_id) {
                        $lostDog->delete();
                        return response()->custom(200, "Successfully deleted lost dog", null);
                    }
                    return response()->custom(403, 'You don\'t have permission to do this', null);
                }
                return response()->custom(404, "Lost dog not found", null);
            } catch (\Exception $e) {
                return response()->custom(404, $e->getMessage(), null);
            }
        } else {
            return TokenHelper::invalidTokenResponse();
        }
    }

    public function search(Request $request)
    {
        $lostDogs = LostDog::Search($request->search)->latest()->paginate(12);
        if(count($lostDogs)) {
            return response()->custom(200, "Successfully get lost dogs", $lostDogs);
        }
        return response()->custom(200, "Successfully get lost dogs", null);
    }
}
