<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helpers\TokenHelper;
use App\Http\Requests\Category\CreateCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        if(count($categories)) {
            return response()->custom(200, "Successfully retrieved categories", $categories);
        }
        return response()->custom(200, "Successfully retrieved categories", null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCategoryRequest $request)
    {
        if(TokenHelper::isValid()) {
            if(TokenHelper::getUserFromToken()->role == "admin") {
                $category = new Category();
                $category->fill($request->all());
                if($category->save()) {
                    return response()->custom(200, "Successfully created category", $category);
                }
                return response()->custom(400, "Something went wrong when creating category", null);
            }
            return response()->custom(403, "You don't have permission to do this", null);
        }
        return TokenHelper::invalidTokenResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);
        if($category) {
            return response()->custom(200, "Successfully retrieved category", $category);
        }
        return response()->custom(404, "Category not found", null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Category $id)
    {
        if(TokenHelper::isValid()) {
            if(TokenHelper::getUserFromToken()->role == "admin") {
                $category = Category::find($id);
                if($category) {
                    $category->fill($request->all());
                    if($category->save()) {
                        return response()->custom(200, "Successfully updated category", $category);
                    }
                    return response()->custom(400, "Something went wrong when updating category", null);
                }
                return response()->custom(404, "Category not found", null);
            }
            return response()->custom(403, "You don't have permission to do this", null);
        }
        return TokenHelper::invalidTokenResponse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
