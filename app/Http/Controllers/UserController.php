<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\ImageService;
use App\Helpers\TokenHelper;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\User\EditUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Image;
use App\LostDog;
use App\Post;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('image')->latest()->paginate(12);
        if(count($users)) {
            return response()->custom(200, 'Successfully get list of users', $users);
        } else {
            return response()->custom(200, 'Successfully get list of users', null);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterRequest $request)
    {
        $user = new User();
        $user->fill($request->all());
        $user->role = "user";
        if($user->save()) {
            $user_avatar = new Image();
            $user_avatar->imageable_id = $user->id;
            $user_avatar->imageable_type = Constant::USER_IDENTIFIER;
            if($request->hasFile('avatar')) {
                $user_avatar->path = ImageService::upload($request->file('avatar'), Constant::USER_IMAGES_ROOT_PATH, $user->id, $request->file('avatar')->getClientOriginalName());
            } else {
                $user_avatar->path = Constant::DEFAULT_USER_IMAGE_PATH;
            }
            $user_avatar->save();
            $credentials = $request->only('email', 'password');
            $token = TokenHelper::login($credentials);
            $user = User::with('image')->find($user->id);
            $entity = array('token' => $token, 'user' => $user);
            return response()->custom(200, 'Successfully created user', $entity);
        }
        return response()->custom(400, 'Something went wrong', null);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('image')->find($id);
        if($user) {
            return response()->custom(200, 'Successfully get user', $user);
        } else {
            return response()->custom(404, 'User not found', null);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        if(TokenHelper::isValid()) {
            if(TokenHelper::getUserFromToken()->id == $id || TokenHelper::getUserFromToken()->role == "admin") {
                $user = User::find($id);
                if($user) {
                    $user->fill($request->all());
                    if(TokenHelper::getUserFromToken()->role != "admin" || !$request->exists('role')) {
                        $user->role = "user";
                    }
                    if($request->hasFile('avatar')) {
                        $user_avatar = Image::where([['imageable_id', '=', $user->id], ['imageable_type', '=', Constant::USER_IDENTIFIER]])->first();
                        if($user_avatar && $user_avatar->path != Constant::DEFAULT_USER_IMAGE_PATH) {
                            ImageService::delete($user_avatar->path, Constant::USER_IMAGES_ROOT_PATH, $user->id);
                        }
                        $user_avatar->path = ImageService::upload($request->file('avatar'), Constant::USER_IMAGES_ROOT_PATH, $user->id, $request->file('avatar')->getClientOriginalName());
                        $user_avatar->save();
                    }
                    $user->save();
                    return response()->custom(200, 'User successvully updated', User::with('image')->find($user->id));
                }
                return response()->custom(404, 'User not found', null);
            }
            return response()->custom(403, 'You don\'t have permission to do this', null);
        }
        return TokenHelper::invalidTokenResponse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(TokenHelper::isValid()) {
            if(TokenHelper::getUserFromToken()->id == $id) {
                $user = User::find($id);
                $posts = Post::where('user_id', $user->id)->get();
                foreach ($posts as $post)
                {
                    $post->delete();
                }
                $lostDogs = LostDog::where('user_id', $user->id)->get();
                foreach ($lostDogs as $lostDog)
                {
                    $lostDog->delete();
                }
                if($user && $user->delete()) {
                    return response()->custom(200, 'Successfully deleted user', null);
                }
                return response()->custom(404, 'User not found', null);
            }
            return response()->custom(403, 'You don\'t have permission to do this', null);
        }
        return TokenHelper::invalidTokenResponse();
    }

    public function getPosts($id)
    {
        $posts = Post::with('images')->where('user_id', $id)->latest()->paginate(10);
        if(count($posts)) {
            return response()->custom(200, 'Successfully retreived user\'s posts', $posts);
        }
        return response()->custom(200, 'Successfully retreived user\'s posts', null);
    }

    public function getLostDogs($id)
    {
        $lostDogs = LostDog::with('images')->where('user_id', $id)->latest()->paginate(10);
        if(count($lostDogs)) {
            return response()->custom(200, 'Successfully retreived user\'s lost dogs', $lostDogs);
        }
        return response()->custom(200, 'Successfully retreived user\'s lost dogs', null);
    }

    public function createUser(RegisterRequest $request)
    {
        if(TokenHelper::isValid()) {
            if(TokenHelper::getUserFromToken()->role == "admin") {
                $user = new User();
                $user->fill($request->all());
                if(TokenHelper::getUserFromToken()->role != "admin" || !$request->exists('role')) {
                    $user->role = "user";
                }
                if($user->save()) {
                    $user_avatar = new Image();
                    $user_avatar->imageable_id = $user->id;
                    $user_avatar->imageable_type = Constant::USER_IDENTIFIER;
                    if($request->hasFile('avatar')) {
                        $user_avatar->path = ImageService::upload($request->file('avatar'), Constant::USER_IMAGES_ROOT_PATH, $user->id, $request->file('avatar')->getClientOriginalName());
                    } else {
                        $user_avatar->path = Constant::DEFAULT_USER_IMAGE_PATH;
                    }
                    $user_avatar->save();
                    return response()->custom(200, 'Successfully created user', User::with('image')->find($user->id));
                }
                return response()->custom(400, 'Something went wrong', null);
            } else {
                return response()->custom(403, 'You don\'t have permission to do this', null);
            }
        }
        return TokenHelper::invalidTokenResponse();
    }

    public function search(Request $request)
    {
        $users = User::Search($request->search)->latest()->paginate(12);
        if(count($users)) {
            return response()->custom(200, 'Successfully get list of users', $users);
        } else {
            return response()->custom(200, 'Successfully get list of users', null);
        }
    }
}
