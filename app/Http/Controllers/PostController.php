<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\ImageService;
use App\Helpers\TokenHelper;
use App\Http\Requests\Post\CreatePostRequest;
use App\Http\Requests\Post\UpdatePostRequest;
use App\Image;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('user', 'images')->latest()->paginate(12);
        if(count($posts)) {
            return response()->custom(200, 'Successfully received posts', $posts);
        }
        return response()->custom(200, 'Successfully received posts', null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        if(TokenHelper::isValid()) {
            try {
                $post = new Post();
                $post->fill($request->all());
                $post->user_id = TokenHelper::getUserFromToken()->id;
                if($post->save()) {
                    $image = new Image();
                    $image->imageable_id = $post->id;
                    $image->imageable_type = Constant::POST_IDENTIFIER;
                    if($request->hasFile('image')) {
                        $image->path = ImageService::upload($request->file('image'), Constant::POSTS_IMAGES_ROOT_PATH, $post->id, $request->file('image')->getClientOriginalName());
                    } else {
                        $image->path = Constant::DEFAULT_POST_IMAGE_PATH;
                    }
                    $image->save();
                    return response()->custom(200, 'Successfully created post', Post::with('user', 'images')->find($post->id));
                } else {
                    return response()->custom(200, 'Post wasn\'t created', null);
                }
            } catch(\Exception $e) {
                return response()->custom(400, $e->getMessage(), null);
            }

        } else {
            return TokenHelper::invalidTokenResponse();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::with('user', 'images')->find($id);
        if($post) {
            return response()->custom(200, 'Successfully recevied post', $post);
        }
        return response()->custom(404, 'Post not found', null);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, $id)
    {
        if(TokenHelper::isValid()) {
            $post = Post::find($id);
            if($post) {
                if(TokenHelper::getUserFromToken()->id == $post->user_id) {
                    $post->fill($request->all());
                    if($request->hasFile('image')) {
                        $post_image = Image::where([['imageable_id', '=', $post->id], ['imageable_type', '=', Constant::POST_IDENTIFIER]])->first();
                        if($post_image && $post_image->path != Constant::DEFAULT_POST_IMAGE_PATH) {
                            ImageService::delete($post_image->path, Constant::POSTS_IMAGES_ROOT_PATH, $post->id);
                        }
                        $post_image->path = ImageService::upload($request->file('image'), Constant::POSTS_IMAGES_ROOT_PATH, $post->id, $request->file('image')->getClientOriginalName());
                        $post_image->save();
                    }
                    $post->save();
                    return response()->custom(200, 'Successfully updated post', Post::with('user', 'images')->find($post->id));
                }
                return response()->custom(403, 'You don\'t have permission to do this', null);
            }
            return response()->custom(404, 'Post not found', null);
        } else {
            return TokenHelper::invalidTokenResponse();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(TokenHelper::isValid()) {
            $post = Post::find($id);
            if($post) {
                if(TokenHelper::getUserFromToken()->id == $post->user_id) {
                    $post->delete();
                    return response()->custom(200, 'Successfully deleted post', null);
                }
                return response()->custom(403, 'You don\'t have permission to do this', null);
            }
            return response()->custom(404, 'Post not found', null);
        } else {
            return TokenHelper::invalidTokenResponse();
        }
    }

    public function search(Request $request)
    {
        $posts = Post::Search($request->search)->latest()->paginate(12);
        if(count($posts)) {
            return response()->custom(200, 'Successfully received posts', $posts);
        }
        return response()->custom(200, 'Successfully received posts', null);
    }
}
