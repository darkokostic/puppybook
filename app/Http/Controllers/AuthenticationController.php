<?php

namespace App\Http\Controllers;

use App\Helpers\TokenHelper;
use App\Http\Requests\Auth\LoginRequest;
use App\User;

class AuthenticationController extends Controller
{
    public function login(LoginRequest $request)
    {
       $credentials = $request->only('email', 'password');
       $token = TokenHelper::login($credentials);
       if($token) {
           $user = User::with('image')->where('email', $request->get('email'))->first();
           $entity = array('token' => $token, 'user' => $user);
           return response()->custom(200, "Successfully logged in", $entity);
       } else {
           $userEmail = User::all()->where('email', $credentials['email']);
           $userPassword = User::all()->where('password', $credentials['password']);
           if(!count($userEmail)) {
               return response()->custom(400, "Invalid email", null);
           } else if(!count($userPassword)) {
               return response()->custom(400, "Invalid password", null);
           } else {
               return response()->custom(400, "Invalid credentials", null);
           }
       }
    }

    public function logout()
    {
        if(TokenHelper::isValid()) {
            return TokenHelper::logout();
        }
        return TokenHelper::invalidTokenResponse();
    }
}
