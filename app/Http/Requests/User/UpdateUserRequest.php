<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'unique:users|string',
            'first_name' => 'string',
            'last_name' => 'string',
            'email' => 'unique:users|email',
            'password' => 'min:4',
            'state' => 'string',
            'city' => 'string',
            'address' => 'string',
            'phone' => 'string',
            'role' => 'string|in:admin,user',
        ];
    }

    public function response(array $errors)
    {
        return response()->custom(400, 'There was an error updating user', $errors);
    }
}
