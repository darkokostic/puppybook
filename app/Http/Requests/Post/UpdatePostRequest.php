<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kind' => 'string',
            'gender' => 'string',
            'price' => 'numeric',
            'papers' => 'boolean'
        ];
    }

    public function response(array $errors)
    {
        return response()->custom(400, 'There was an error updating post', $errors);
    }
}
