<?php

namespace App\Helpers;


use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class TokenHelper
{
    public static function logout()
    {
        JWTAuth::invalidate(JWTAuth::getToken());
        return response()->custom(200, 'Successfully logged out', null);
    }

    public static function login($credentials)
    {
        return JWTAuth::attempt($credentials);
    }

    public static function getUserFromToken()
    {
        return User::all()->find(JWTAuth::parseToken()->authenticate()->id);
    }

    public static function isValid()
    {
        try {
            JWTAuth::parseToken()->authenticate();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function invalidTokenResponse()
    {
        try {
            JWTAuth::parseToken()->authenticate();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->custom(401, 'Token expired', null);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->custom(401, 'Token invalid', null);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->custom(401, 'Token invalid', null);
        }
    }
}