<?php
/**
 * Created by PhpStorm.
 * User: darko
 * Date: 22.7.17.
 * Time: 11.47
 */

namespace App\Helpers;


class Constant
{
    const USER_IDENTIFIER = 'App\User';
    const POST_IDENTIFIER = 'App\Post';
    const LOST_DOG_IDENTIFIER = 'App\LostDog';

    const IMAGES_ROOT_PATH = 'images/uploads/';
    const USER_IMAGES_ROOT_PATH = Constant::IMAGES_ROOT_PATH.'users/';
    const POSTS_IMAGES_ROOT_PATH = Constant::IMAGES_ROOT_PATH.'posts/';
    const LOST_DOGS_IMAGES_ROOT_PATH = Constant::IMAGES_ROOT_PATH.'lost-dogs/';

    const DEFAULT_USER_IMAGE_PATH =  Constant::USER_IMAGES_ROOT_PATH.'user-avatar.jpg';
    const DEFAULT_POST_IMAGE_PATH =  Constant::POSTS_IMAGES_ROOT_PATH.'placeholder.png';
}