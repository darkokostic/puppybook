<?php
/**
 * Created by PhpStorm.
 * User: darko
 * Date: 22.7.17.
 * Time: 12.28
 */

namespace App\Helpers;


use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImageService
{
    public static function upload($image_file, $rootPath, $id, $file_name)
    {
         if(!file_exists(public_path($rootPath.$id)))
         {
             mkdir(public_path($rootPath.$id), 777, true);
         }
         $path = $rootPath.$id.'/'.Carbon::now()->format("d:m:Y:h:m:s").$file_name;
         Image::make($image_file)->save($path);
         return $path;
    }

    public static function delete($path, $rootPath, $id)
    {
        Storage::delete($path);
        $files = Storage::allFiles($rootPath.$id);
        if(!$files)
        {
            File::deleteDirectory(public_path($rootPath.$id));
        }
    }
}