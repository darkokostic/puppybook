<?php

namespace App;

use App\Helpers\Constant;
use App\Helpers\ImageService;
use Illuminate\Database\Eloquent\Model;

class LostDog extends Model
{
    protected $fillable = [
        'name', 'description', 'kind', 'gender', 'location', 'phone'
    ];

    public function category() {
        return $this->hasOne(Category::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function delete()
    {
        $images = Image::where([['imageable_id', $this->id], ['imageable_type', Constant::LOST_DOG_IDENTIFIER]])->get();
        foreach ($images as $image)
        {
            if($image->path != Constant::DEFAULT_POST_IMAGE_PATH) {
                ImageService::delete($image->path, Constant::LOST_DOGS_IMAGES_ROOT_PATH, $this->id);
            }
            $image->delete();
        }
        return parent::delete();
    }

    public function scopeSearch($query, $keyword)
    {
        if($keyword != '') {
            $query->with('images')->where(function($query) use ($keyword) {
                $query->where('name', 'LIKE', "%$keyword%")
                    ->orWhere('kind', 'LIKE', "%$keyword%")
                    ->orWhere('description', 'LIKE', "%$keyword%")
                    ->orWhere('gender', 'LIKE', "%$keyword%")
                    ->orWhere('location', 'LIKE', "%$keyword%")
                    ->orWhere('phone', 'LIKE', "%$keyword%");
            });
        }
        return $query;
    }
}
