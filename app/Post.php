<?php

namespace App;

use App\Helpers\Constant;
use App\Helpers\ImageService;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'kind', 'description', 'gender', 'price', 'papers'
    ];

    public function category() {
        return $this->hasOne(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function delete()
    {
        $images = Image::where([['imageable_id', $this->id], ['imageable_type', Constant::POST_IDENTIFIER]])->get();
        foreach ($images as $image)
        {
            if($image->path != Constant::DEFAULT_POST_IMAGE_PATH) {
                ImageService::delete($image->path, Constant::POSTS_IMAGES_ROOT_PATH, $this->id);
            }
            $image->delete();
        }
        return parent::delete();
    }

    public function scopeSearch($query, $keyword)
    {
        if($keyword != '') {
            $query->with('images')->where(function($query) use ($keyword) {
                $query->where('kind', 'LIKE', "%$keyword%")
                    ->orWhere('description', 'LIKE', "%$keyword%")
                    ->orWhere('gender', 'LIKE', "%$keyword%")
                    ->orWhere('price', 'LIKE', "%$keyword%")
                    ->orWhere('papers', 'LIKE', "%$keyword%");
            });
        }
        return $query;
    }
}
