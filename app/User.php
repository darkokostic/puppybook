<?php

namespace App;

use App\Helpers\Constant;
use App\Helpers\ImageService;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'first_name', 'last_name', 'email', 'password', 'role', 'city', 'state', 'address', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }

    public function delete()
    {
        $images = Image::where([['imageable_id', $this->id], ['imageable_type', Constant::USER_IDENTIFIER]])->get();
        foreach ($images as $image)
        {
            if($image->path != Constant::DEFAULT_USER_IMAGE_PATH) {
                ImageService::delete($image->path, Constant::USER_IMAGES_ROOT_PATH, $this->id);
            }
            $image->delete();
        }
        return parent::delete();
    }

    public function scopeSearch($query, $keyword)
    {
        if($keyword != '') {
            $query->with('image')->where(function ($query) use ($keyword) {
                $query->where('username', 'LIKE', "%$keyword%")
                    ->orWhere("first_name", "LIKE", "%$keyword%")
                    ->orWhere("last_name", "LIKE", "%$keyword%")
                    ->orWhere("email", "LIKE", "%$keyword%")
                    ->orWhere("city", "LIKE", "%$keyword%")
                    ->orWhere("state", "LIKE", "%$keyword%")
                    ->orWhere("address", "LIKE", "%$keyword%");
            });
        }
        return $query;
    }
}
