<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i < 100; $i++) {
            DB::table('posts')->insert([
                'user_id' => rand(1, 102),
                'category_id' => rand(1, 10),
                'kind' => $faker->word,
                'gender' => 'male',
                'description' => $faker->text,
                'price' => $faker->numberBetween(50, 3000),
                'papers' => $faker->boolean,
                'created_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
