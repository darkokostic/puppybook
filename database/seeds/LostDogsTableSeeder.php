<?php

use Illuminate\Database\Seeder;

class LostDogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for($i = 0; $i < 100; $i++)
        {
            DB::table('lost_dogs')->insert([
                'user_id' => rand(1, 102),
                'category_id' => rand(1, 10),
                'name' => $faker->firstName,
                'description' => $faker->text(50),
                'kind' => "Buldog",
                'gender' => "male",
                'location' => $faker->streetAddress,
                'phone' => $faker->phoneNumber
            ]);
        }
    }
}
