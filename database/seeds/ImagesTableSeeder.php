<?php

use Illuminate\Database\Seeder;
use App\Helpers\Constant;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i < 103; $i++)
        {
            DB::table('images')->insert([
                'imageable_id' => $i,
                'imageable_type' => Constant::USER_IDENTIFIER,
                'path' => Constant::DEFAULT_USER_IMAGE_PATH,
                'created_at' => \Carbon\Carbon::now()
            ]);
        }

        for($i = 1; $i < 101; $i++)
        {
            DB::table('images')->insert([
                'imageable_id' => $i,
                'imageable_type' => Constant::POST_IDENTIFIER,
                'path' => Constant::DEFAULT_POST_IMAGE_PATH,
                'created_at' => \Carbon\Carbon::now()
            ]);
        }

        for($i = 1; $i < 101; $i++)
        {
            DB::table('images')->insert([
                'imageable_id' => $i,
                'imageable_type' => Constant::LOST_DOG_IDENTIFIER,
                'path' => Constant::DEFAULT_POST_IMAGE_PATH,
                'created_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
