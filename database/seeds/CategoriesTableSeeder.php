<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Maltezer',
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('categories')->insert([
            'name' => 'Pomeranski špic',
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('categories')->insert([
            'name' => 'Nemački ovčar',
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('categories')->insert([
            'name' => 'Staford',
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('categories')->insert([
            'name' => 'Haski',
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('categories')->insert([
            'name' => 'Labrador retriver',
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('categories')->insert([
            'name' => 'Kane korso',
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('categories')->insert([
            'name' => 'Kovrdžavi bišon',
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('categories')->insert([
            'name' => 'Rotvajler',
            'created_at' => \Carbon\Carbon::now()
        ]);

        DB::table('categories')->insert([
            'name' => 'Pudla - patuljasta',
            'created_at' => \Carbon\Carbon::now()
        ]);
    }
}
