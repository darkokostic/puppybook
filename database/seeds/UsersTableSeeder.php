<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => "admin",
            'first_name' => "Admin",
            'last_name' => "Admin",
            'email' => "admin@demo.com",
            'password' => bcrypt('123'),
            'city' => "City",
            'state' => "State",
            'address' => "Address",
            'phone' => '064555333',
            'role' => 'admin'
        ]);

        DB::table('users')->insert([
            'username' => "user",
            'first_name' => "User",
            'last_name' => "User",
            'email' => "user@demo.com",
            'password' => bcrypt('123'),
            'city' => "City",
            'state' => "State",
            'address' => "Address",
            'phone' => '064555333',
            'role' => 'user'
        ]);

        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 100; $i++)
        {
            DB::table('users')->insert([
                'username' => $faker->unique()->userName,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'email' => $faker->unique()->email,
                'password' => bcrypt('123'),
                'city' => $faker->city,
                'state' => $faker->country,
                'address' => $faker->address,
                'phone' => $faker->phoneNumber,
                'role' => 'user'
            ]);
        }
    }
}
